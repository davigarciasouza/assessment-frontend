# WEBJUMP FRONTEND CHALLENGE

O desafio consiste em tornar o layout proposto em uma página funcional.

### Tecnologias adotadas
Todas tecnologias foram escolhidas a fim de trazer velocidade ao desenvolvimento.

-Vuejs: Framework simples que permite alta produtividade.

-Bootstrap: mesmo motivo anterior, alta produtividade, principalmente para designs responsivos.

-axios: lib simples e fácil de usar para realizar requisições na API.

-vue-router: para facilitar o gerenciamento das rotas da aplicação.

### Para rodar o projeto localmente

1. Clonar o repósitório
2. Rodar no terminal o seguinte comando: `./start.sh`
3. Acessar o localhost:8888 no navegador

*Em caso de erro, rodar no terminal a sequência de comandos a seguir a partir da pasta raiz do repositório

4. `cd base-project`
5. `npm install`
6. `npm start` para rodar localmente a api do projeto
7. Em um novo terminal a partir da raiz do projeto `cd frontend-challenge`
8. `npm install`
9. `npm run serve` para rodar localmente o frontend da aplicação.


## Checklist
### Requisitos
✅ JavaScript para consultar a lista de categorias a serem exibidas no menu e lista de produtos das categorias

✅ Fonte padrão: "Open Sans" e "Open Sans - Extrabold"

✅ Design responsivo nos breakpoints 320px, 768px, 1024px e 1440px

✅ Suporte para IE, Chrome, Firefox. Não testado no Safari 

### Diferenciais
✅ Uso de pré-processadores CSS (Sass, Less) - foi utilizado SCSS

✅ Fazer os filtros da sidebar funcionarem através de Javascript

### Funcionalidade não exigida

✅ Visualização em lista em alternativa aos cards de produtos

- A aplicação agora tem a opção de visualizar os cards com um layout alternativo no desktop, 
embora seja possível alternar para lista e pelo devtools visualizar como ficaria no mobile, 
a melhor opção para mobile continua sendo em formato de cards.

