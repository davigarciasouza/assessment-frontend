#!/bin/sh

cd frontend-challenge
npm install && npm run build
cd ../base-project
rm -rf public/*
cp -r ../frontend-challenge/dist/* public/
npm install && npm start
